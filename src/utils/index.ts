import { googleSrc } from "../assets";

export const logos: Record<string, File> = {
  google: googleSrc,
};
