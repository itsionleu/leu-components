import { ReactNode } from "react";

type DrawerStateType = "create" | "update" | "closed";

export type DrawerPropsType = {
  drawerState: DrawerStateType;
  showDrawerHeader?: boolean;
  drawerTitle?: string;
  children: ReactNode;
  onClose?: () => void;
  maxWidth?: string;
};
