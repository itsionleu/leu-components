import React from "react";
import { FC } from "react";
import { DrawerPropsType } from "./Drawer.type";
import "./Drawer.css";

const Drawer: FC<DrawerPropsType> = (props) => {
  const {
    drawerState,
    showDrawerHeader = true,
    drawerTitle,
    children,
    onClose,
    maxWidth = "32rem",
  } = props;

  return drawerState !== "closed" ? (
    <div className="relative z-10">
      <div className="backdrop"></div>
      <div className="overflow-container">
        <div className="inner-container">
          <div className="slide-panel-container">
            <div className="slide-panel">
              <div className="panel-content" style={{ maxWidth: maxWidth }}>
                {showDrawerHeader && (
                  <div className="header-container">
                    <h2 id="slide-over-title">
                      {drawerTitle ? drawerTitle : "Drawer title"}
                    </h2>
                    <div className="close-button-container">
                      <button
                        type="button"
                        className="close-button"
                        onClick={onClose}
                      >
                        <span className="button-overlay"></span>
                        <span className="sr-only">Close</span>
                        <svg
                          className="icon"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M6 18L18 6M6 6l12 12"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                )}
                <div className="content-area">{children}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default Drawer;
